/* Resources */

var Resource = require('deployd/lib/resource')
    , util = require('util')
    , fs = fs = require('fs')
    , ejs = require('ejs');


function Template(name, options) {
    Resource.apply(this, arguments);
}

util.inherits(Template, Resource);
module.exports = Template;

Template.prototype.clientGeneration = true;

Template.prototype.handle = function (ctx, next) {
    if(ctx.req && ctx.req.method !== 'POST') return next();
    try {
        /* Get template */
        fs.readFile('templates/'+ctx.body.template, {encoding: 'utf-8'}, function(error, html) {
            /* Generate final template with data send by deployd event */
            var template = ejs.render(html, ctx.body.data);
            /* return template */
            ctx.done(null, {html: template});
        });
    } catch (ex) {
        console.log(ex);
    }
}