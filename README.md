# DPD-TEMPLATE
#### dpd-template is deployd resource to generate an HTML template with data, it uses EJS to have a dynamical templating system.
---

### Templates folder
It's easy to use, you just have to put your templates in a folder **templates** at the root of your deployd project.

├── app.dpd  
├── node_modules  
├── public  
├── resources  
└── templates  
&nbsp;&nbsp;&nbsp;&nbsp;└── register.html  
&nbsp;&nbsp;&nbsp;&nbsp;└── forgot.html  
&nbsp;&nbsp;&nbsp;&nbsp;└── forgot.html      


### Examples 

Next you have to add it in your event, here an example of validation mail with **dpd-email** on the event **POST** for my users collection :

```js

dpd.template.post({"template": 'register.html', "data": {user: this}}, function(data) {
    dpd.email.post({
      to      : "xxxx@xxxxxxx.xx",
      subject : 'My subject',
      html    : data.html
    }, function ( err, results ) {
        console.log(results);
    });
});
```

So the main part is 

```js
dpd.template.post({"template": 'register.html', "data": {user: this}}, function(data) {
});
```
And it will return an object with the attributes ***html***, this attribute contains your template generated.

### EJS
If you want learn to user EJS you can go here http://www.embeddedjs.com/

### About me

If you want to offer me a beer

[![Foo](http://www.oscostarica.com/wp-content/uploads/2014/09/paypal2.jpg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=E7C6TDAHHGZ9N)
